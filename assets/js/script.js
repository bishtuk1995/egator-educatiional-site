
//scroll
$(document).ready(()=> { 
    $(window).scroll(function(){
        var top__navbar = $('#top__navbar'),
            scroll = $(window).scrollTop();
             
            
        // console.log('scroll', scroll)

        if (scroll >= 80) top__navbar.addClass('nav-fixed');
        else top__navbar.removeClass('nav-fixed');
      })
      
});


//accordioan

let faqs= document.querySelectorAll(".faq");

 faqs.forEach(element => {
      element.addEventListener("click", (e) => { 
         element.classList.toggle("show");

         //change icon (<box-icon name='minus'></box-icon>)
         const icon = element.querySelector(".faq__icon i");
         console.log(icon.className        )
         if (icon.className=='bx bx-plus-circle') {
             icon.className='bx bx-minus';
         } else {
            icon.classList='bx bx-plus-circle';
             
         }
       });
 });


//  humburger
const humburgerBtn = document.querySelector("#humburger")

humburgerBtn.addEventListener('click',() => { 
  console.log('ok')
 
   humburgerBtn.classList.toggle("is-active");

   let nav_menu=  document.querySelector("#top__navbar .nav__menu");
   nav_menu.classList.toggle("hamburger-active-class")
    if (nav_menu.classList.contains("hamburger-active-class")    ) {
       nav_menu.style.display='block'
    } else {
      nav_menu.style.display='none'

    }
 })





 //swipper 
 var swiper = new Swiper(".mySwiper", {
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints:{
        600:{
          slidesPerView: 3,
        }
    }
  });
  

